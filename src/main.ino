#include <BleKeyboard.h>
#include <Bounce2.h>

#define PINS 5
int pins[PINS] = {12, 14, 27, 26, 25};

BleKeyboard bleKeyboard("keyyyyyyyys");
Bounce debouncers[PINS] = {
    Bounce(),
    Bounce(),
    Bounce(),
    Bounce(),
    Bounce()
};

int Keys [31] = {
    'o',
    'a',
    'l',
    't',
    'u',
    'd',
    'q',
    'e',
    'f',
    'w',
    'x',
    'r',
    'j',
    'p',
    KEY_BACKSPACE,
    ' ',
    'h',
    's',
    'k',
    'n',
    'v',
    ',',
    'm',
    'i',
    'b',
    'y',
    '.',
    'c',
    'z',
    'g',
    KEY_RETURN,
};

struct State {
    int last;
    int current;
    int max;
} state;

bool updateDebouncers() {
    bool changed = false;
    for (int i = 0; i < PINS; i++) {
        if (debouncers[i].update()) {
            changed = true;
            break;
        }
    }
    return changed;
}

int getState() {
    int s = 0;
    for (int i = 0; i < PINS; i++) {
        s |= (bool(debouncers[i].read()) << i);
    }
    return s;
}

void setup() {
    for (int i = 0; i < PINS; i++) {
        pinMode(pins[i], INPUT_PULLDOWN);
        debouncers[i].attach(pins[i]);
        debouncers[i].interval(5);
    }

    Serial.begin(115200);
    Serial.println("Starting BLE work!");

    pinMode(22, OUTPUT);
    digitalWrite(22, HIGH);

    bleKeyboard.begin();
}

void sendKey(int key) {
    bleKeyboard.write(Keys[key - 1]);
}

void stateChanged() {
    if (state.current == 0) {
        digitalWrite(22, HIGH);
        sendKey(state.max);
        state.max = state.current = state.last = 0;
    } else if (state.current > state.max) {
        state.max = state.current;
        digitalWrite(22, LOW);
    }
}

void loop() {
    if (!bleKeyboard.isConnected()) {
        delay(10);
        return;
    }
    if (updateDebouncers()) {
        state.current = getState();
        if (state.current != state.last) {
            state.last = state.current;
            stateChanged();
        }
    } else {
        delay(1);
    }
}
